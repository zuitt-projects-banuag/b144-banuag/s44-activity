//Get post data using fetch()
//fetching the url para malagay yung data
//Kapag nag fetch should have a promise condition
fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((data) => showPosts(data));

//Add post data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	//Prevents the page from loading
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		//Identify the method first, to identify the placeholder
		method: 'POST',

		//body of JSON Data
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		//Headers of JSON Data
		headers: {'Content-Type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json()) //returning response in JSON data
	.then((data) => {
		console.log(data);
		alert('Successfully added.');

		//Clears the text elements upon post creation
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	})
})

//Show Posts

const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	});

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

//Edit Post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	//Removes the disabled attribute from the button
	document.querySelector('#btn-submit-update').removeAttribute('disabled')
}

//Update Post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		//Identify the method first, to identify the placeholder
		method: 'PUT',

		//body of JSON Data
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		//Headers of JSON Data
		headers: {'Content-Type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json()) //returning response in JSON data
	//to display new data update
	.then((data) => {
		console.log(data);
		alert('Successfully updated.');

		//Clears the text elements upon post creation
		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		//Setting back the disabled attribute to true
		document.querySelector('#btn-submit-update').setAttribute('disabled', true);
	})
})

//Delete Post

const deletePost = (id) => {
    document.querySelector(`#post-${id}`).remove();
};
